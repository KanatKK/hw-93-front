import {
    REGISTER_USER,
    USER_FAILURE,
    REGISTER_USER_SUCCESS,
    LOGIN_USER_SUCCESS,
    LOGIN_USER,
    LOGOUT_USER,
    FACEBOOK_LOGIN,
    GET_EVENTS,
    CREATE_EVENTS,
    EVENT_FAILURE,
    GET_EVENTS_SUCCESS,
    DELETE_EVENT,
    GET_USER_BY_EMAIL,
    GET_USER_BY_EMAIL_SUCCESS,
    GET_USER_BY_EMAIL_FAILURE,
    SHARE_WITH_USER,
    GET_SHARED_USERS,
    GET_SHARED_USERS_SUCCESS, RELOAD_USER, DELETE_USER
} from "./actionTypes";

export const registerUserSuccess = user => {
    return {type: REGISTER_USER_SUCCESS, user};
};
export const userFailure = error => {
    return {type: USER_FAILURE, error};
};
export const registerUser = userData => {
    return {type: REGISTER_USER, userData};
};

export const loginUser = userData => {
    return {type: LOGIN_USER, userData};
};
export const loginUserSuccess = user => {
    return {type: LOGIN_USER_SUCCESS, user};
};

export const logoutUser = (user) => {
    return {type: LOGOUT_USER, user};
};
export const facebookLogin = data => {
    return {type: FACEBOOK_LOGIN, data};
};

export const getEvents = (user) => {
    return {type: GET_EVENTS, user};
};
export const getEventsSuccess = (events) => {
    return {type: GET_EVENTS_SUCCESS, events}
}
export const createEvent = data => {
    return {type: CREATE_EVENTS, data};
};
export const eventFailure = error => {
    return {type: EVENT_FAILURE, error};
};
export const deleteEvent = id => {
    return {type: DELETE_EVENT, id};
};

export const getUserByEmail = user => {
    return {type: GET_USER_BY_EMAIL, user};
};
export const getUserByEmailSuccess = user => {
    return {type: GET_USER_BY_EMAIL_SUCCESS, user};
};
export const getUserByEmailFailure = error => {
    return{type: GET_USER_BY_EMAIL_FAILURE, error};
};

export const shareWithUser = user => {
    return{type: SHARE_WITH_USER, user};
};
export const getSharedUsers = user => {
    return{type: GET_SHARED_USERS, user};
};
export const getSharedUsersSuccess = users => {
    return{type: GET_SHARED_USERS_SUCCESS, users};
};
export const reloadUser = user => {
    return{type: RELOAD_USER, user};
};

export const deleteUser = user => {
    return{type: DELETE_USER, user};
};