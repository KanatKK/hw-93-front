import {takeEvery} from "redux-saga/effects";
import {
    deleteUserSaga,
    facebookLoginSaga, getSharedUsersSaga,
    getUserByEmailSaga,
    loginUserSaga,
    logoutUserSaga,
    registerUserSaga, reloadUserSaga,
    shareWithUserSaga
} from "./user";
import {
    CREATE_EVENTS,
    DELETE_EVENT, DELETE_USER,
    FACEBOOK_LOGIN,
    GET_EVENTS, GET_SHARED_USERS, GET_USER_BY_EMAIL,
    LOGIN_USER,
    LOGOUT_USER,
    REGISTER_USER, RELOAD_USER, SHARE_WITH_USER
} from "../actionTypes";
import {createEventSaga, deleteEventSaga, getEventsSaga} from "./events";

export function* rootSaga() {
    yield takeEvery(REGISTER_USER, registerUserSaga);
    yield takeEvery(LOGIN_USER, loginUserSaga);
    yield takeEvery(LOGOUT_USER, logoutUserSaga);
    yield takeEvery(FACEBOOK_LOGIN, facebookLoginSaga);
    yield takeEvery(GET_EVENTS, getEventsSaga);
    yield takeEvery(CREATE_EVENTS, createEventSaga);
    yield takeEvery(DELETE_EVENT, deleteEventSaga);
    yield takeEvery(GET_USER_BY_EMAIL, getUserByEmailSaga);
    yield takeEvery(SHARE_WITH_USER, shareWithUserSaga);
    yield takeEvery(GET_SHARED_USERS, getSharedUsersSaga);
    yield takeEvery(RELOAD_USER, reloadUserSaga);
    yield takeEvery(DELETE_USER, deleteUserSaga);
}