import {put} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {eventFailure, getEventsSuccess, userFailure} from "../actions";

export function* getEventsSaga ({user}) {
    try {
        const response = yield axiosApi.get("/events/" + user);
        yield put(getEventsSuccess(response.data));
    } catch (e) {
        console.log(e);
    }
}

export function* deleteEventSaga ({id}) {
    try {
        yield axiosApi.delete("/events/" + id);
    } catch (e) {
        console.log(e);
    }
}

export function* createEventSaga ({data}) {
    try {
        yield axiosApi.post("/events", data);
    } catch (e) {
        if (e.response && e.response.data) {
            yield put(eventFailure(e.response.data.error));
        } else {
            yield put(userFailure({global: "No internet"}));
        }
    }
}