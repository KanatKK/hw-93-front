import {put} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {
    userFailure,
    registerUserSuccess,
    loginUserSuccess,
    logoutUser,
    getUserByEmailSuccess,
    getUserByEmailFailure, getSharedUsersSuccess
} from "../actions";

export function* registerUserSaga({userData}) {
    try {
        const response = yield axiosApi.post("/users", userData);
        yield put(registerUserSuccess(response.data));
    } catch(e) {
        if (e.response && e.response.data) {
            yield put(userFailure(e.response.data.error));
        } else {
            yield put(userFailure({global: "No internet"}));
        }
    }
}
export function* loginUserSaga({userData}) {
    try {
        const response = yield axiosApi.post("/users/sessions", userData);
        yield put(loginUserSuccess(response.data));
    } catch(e) {
        if (e.response && e.response.data) {
            yield put(userFailure(e.response.data.error));
        } else {
            yield put(userFailure({global: "No internet"}));
        }
    }
}
export function* reloadUserSaga({user}) {
    try {
        const response = yield axiosApi.get("/users/shareWith/" + user);
        yield put(loginUserSuccess(response.data));
    } catch (e) {
        console.log(e);
    }
}
export function* getUserByEmailSaga({user}) {
    try {
        const response = yield axiosApi.get("/users/shareWith/" + user);
        yield put(getUserByEmailSuccess(response.data));
    } catch (e) {
        if (e.response && e.response.data) {
            console.log(e.response.data.error)
            yield put(getUserByEmailFailure(e.response.data.error));
        } else {
            yield put(getUserByEmailFailure({global: "No internet"}));
        }
    }
}
export function* getSharedUsersSaga({user}) {
    try {
        const response = yield axiosApi.get("/users/shared/" + user);
        yield put(getSharedUsersSuccess(response.data));
    } catch (e) {
        console.log(e);
    }
}
export function* shareWithUserSaga({user}) {
    try {
        yield axiosApi.post("/users/shareWith/" + user);
    } catch (e) {
        console.log(e);
    }
}
export function* logoutUserSaga({user}) {
    if (user) {
        yield axiosApi.delete("/users/sessions");
        yield put(logoutUser());
    }
}
export function* facebookLoginSaga({data}) {
    try {
        const response = yield axiosApi.post("/users/facebookLogin", data);
        yield put(loginUserSuccess(response.data));
    } catch(e) {
        yield put(userFailure(e.response.data));
    }
}
export function* deleteUserSaga({user}) {
    try {
        yield axiosApi.delete("/users/shareWith/delete/" + user);
    } catch (e) {
        console.log(e);
    }
}