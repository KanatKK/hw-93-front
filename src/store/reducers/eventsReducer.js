import {EVENT_FAILURE, GET_EVENTS_SUCCESS} from "../actionTypes";

const initialState = {
    events: null,
    errors: "",
};

const events = (state=initialState, action) => {
    switch (action.type) {
        case EVENT_FAILURE:
            return {...state, errors: action.error};
        case GET_EVENTS_SUCCESS:
            return {...state, events: action.events, errors: null};
        default:
            return state;
    }
};

export default events;