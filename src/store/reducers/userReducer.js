import {
    GET_SHARED_USERS_SUCCESS,
    GET_USER_BY_EMAIL_FAILURE,
    GET_USER_BY_EMAIL_SUCCESS,
    LOGIN_USER_SUCCESS,
    LOGOUT_USER,
    REGISTER_USER_SUCCESS,
    USER_FAILURE
} from "../actionTypes";

const initialState = {
    user: null,
    errors: "",
    userByEmail: null,
    userByEmailErrors: "",
    sharedUsers: null,
};

const user = (state = initialState, action) => {
    switch (action.type) {
        case GET_SHARED_USERS_SUCCESS:
            return {...state, sharedUsers: action.users};
        case GET_USER_BY_EMAIL_FAILURE:
            return  {...state, userByEmail: null, userByEmailErrors: action.error};
        case GET_USER_BY_EMAIL_SUCCESS:
            return {...state, userByEmail: action.user, userByEmailErrors: null};
        case USER_FAILURE:
            return {...state, errors: action.error};
        case LOGOUT_USER:
            return {...state, user: null}
        case LOGIN_USER_SUCCESS:
            return {...state, user: action.user, errors: null}
        case REGISTER_USER_SUCCESS:
            return {...state, user: action.user, errors: null};
        default:
            return state;
    }
};

export default user;