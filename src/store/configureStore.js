import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import thunkMiddleware from "redux-thunk";
import createSagaMiddleware from "redux-saga";
import {rootSaga} from "./sagas";
import user from "./reducers/userReducer";
import events from "./reducers/eventsReducer";


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducers = combineReducers({
    user: user,
    events: events,
});

const persistedState = loadFromLocalStorage();

const sagaMiddleware = createSagaMiddleware();

const middleware = [
    thunkMiddleware,
    sagaMiddleware,
];

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const store = createStore(rootReducers, persistedState, enhancers);

sagaMiddleware.run(rootSaga);

store.subscribe(() => {
    saveToLocalStorage({
        user: {
            user: store.getState().user.user,
        }
    });
});

export default store;