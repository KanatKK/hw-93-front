import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getEvents} from "../../store/actions";

const SharedEventList = (props) => {
    const events = useSelector(state => state.events.events);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getEvents(props.id));
    }, [dispatch, props.id]);

    useEffect(() => {
        const interval = setInterval( () => {
            dispatch(getEvents(props.id));
        }, 2000);
        return () => clearInterval(interval);
    }, [dispatch, props.id]);

    if (events) {
        const eventsList = events.map((ev, ind) => {
            return (
                <div className="event" key={ind}>
                    <p className="eventDate">{ev.date}</p>
                    <p>{ev.description}</p>
                </div>
            )
        })
        return (
            <>
                {eventsList}
            </>
        );
    } else {
        return null;
    }
};

export default SharedEventList;