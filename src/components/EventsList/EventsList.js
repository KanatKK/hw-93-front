import React, {useEffect} from 'react';
import {deleteEvent, getEvents} from "../../store/actions";
import {useDispatch, useSelector} from "react-redux";

const EventsList = () => {
    const userId = useSelector(state => state.user.user._id);
    const events = useSelector(state => state.events.events);
    const dispatch = useDispatch();

    useEffect(() => {
        const interval = setInterval( () => {
            dispatch(getEvents(userId));
        }, 2000);
        return () => clearInterval(interval);
    }, [dispatch, userId]);

    const deleteEventHandler = event => {
        dispatch(deleteEvent(event.target.id));
    };

    if (events) {
        const eventsList = events.map((ev, ind) => {
            return (
                <div className="event" key={ind}>
                    <button
                        className="deleteBtn" id={ev._id}
                        onClick={deleteEventHandler}>X</button>
                    <p className="eventDate">{ev.date}</p>
                    <p>{ev.description}</p>
                </div>
            )
        })
        return (
            <>
                {eventsList}
            </>
        );
    } else {
        return null;
    }
};

export default EventsList;