import React from 'react';
import './App.css';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import SignIn from "../Registration/SignIn";
import Login from "../Registration/Login";
import MainPage from "../MainPage/MainPage";
import Shared from "../Shared/Shared";
import UserEvents from "../UserEvents/UserEvents";

const App = () => {
  return (
      <BrowserRouter>
          <Switch>
              <Route path="/" exact component={SignIn}/>
              <Route path="/login" exact component={Login}/>
              <Route path="/mainPage/:id" exact component={MainPage}/>
              <Route path="/shared/:id" exact component={Shared}/>
              <Route path="/userEvents/:id" exact component={UserEvents}/>
          </Switch>
      </BrowserRouter>
  );
};

export default App;
