import React, {useEffect, useState} from 'react';
import RegistrationBlock from "../Registration/RegistrationBlock";
import '@material-ui/core/Typography';
import '@material-ui/core/Button';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {KeyboardDatePicker, MuiPickersUtilsProvider} from '@material-ui/pickers';
import {useDispatch, useSelector} from "react-redux";
import {createEvent, getEvents, getUserByEmail, getUserByEmailSuccess, shareWithUser} from "../../store/actions";
import EventsList from "../../components/EventsList/EventsList";

const MainPage = (props) => {
    const dispatch = useDispatch();
    const errors = useSelector(state => state.events.errors);
    const user = useSelector(state => state.user.user);
    const userByEmail = useSelector(state => state.user.userByEmail);
    const userByEmailErrors = useSelector(state => state.user.userByEmailErrors);
    const [selectedDate, setSelectedDate] = useState(new Date());
    const [description, setDescription] = useState("");
    const [userEmail, setUserEmail] = useState("");

    useEffect(() => {
        dispatch(getEvents(props.match.params.id));
    }, [dispatch, props.match.params.id]);

    const handleDateChange = (date) => {
        setSelectedDate(date);
    };

    const descriptionChanger = event => {
        setDescription(event.target.value);
    };

    const userEmailChanger = event => {
        setUserEmail(event.target.value);
    };

    const createEventHandler = () => {
        const data = {
            date: selectedDate.getUTCFullYear()+ '-' + Number(selectedDate.getMonth() + 1) + '-' + selectedDate.getDate(),
            isoDate: selectedDate.toISOString(),
            description: description,
        };
        dispatch(createEvent(data));
        setDescription("");
    };

    const getUserByEmailHandler = () => {
        dispatch(getUserByEmail(userEmail));
        setUserEmail("");
    };

    const shareWithUserHandler = event => {
        dispatch(shareWithUser(event.target.id));
        dispatch(getUserByEmailSuccess(null));
    };

    if (user) {
        return (
            <div className="container">
                <header>
                    <h2>Main Page</h2>
                    <RegistrationBlock/>
                </header>
                <div className="content">
                    <div className="mainPage">
                        <div className="form">
                            <div className="datePicker">
                                <p>Choose date:</p>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <KeyboardDatePicker
                                        margin="normal"
                                        id="date-picker-dialog"
                                        label="Date picker dialog"
                                        format="MM/dd/yyyy"
                                        value={selectedDate}
                                        onChange={handleDateChange}
                                        KeyboardButtonProps={{
                                            'aria-label': 'change date',
                                        }}
                                    />
                                </MuiPickersUtilsProvider>
                            </div>
                            <div className="eventBlock">
                                <p>Write an event:</p>
                                <textarea className="eventTxt" value={description} onChange={descriptionChanger}/>
                                <button className="sendBtn" onClick={createEventHandler}>Send</button>
                                {errors && <span className="error">{errors}</span>}
                                <div className="hover"/>
                                <div className="shareBlock">
                                    <label>
                                        Find user:
                                        <input
                                            className="findUserField"
                                            type="text" placeholder="Email"
                                            onChange={userEmailChanger} value={userEmail}
                                        />
                                    </label>
                                    <button
                                        type="button" onClick={getUserByEmailHandler}
                                        className="findUserBtn">Find</button>
                                    {userByEmailErrors && <p className="error">{userByEmailErrors}</p>}
                                    {
                                        userByEmail &&
                                            <div className="userByInfo">
                                                <div className="userByInfoInfo">
                                                    <h5>{userByEmail.displayName}</h5>
                                                    <div className="userByInfoImg">
                                                        <img src={userByEmail.imageLink} alt=""/>
                                                    </div>
                                                </div>
                                                <button
                                                    className="shareBtn" type="button"
                                                    id={userByEmail._id} onClick={shareWithUserHandler}
                                                >Share</button>
                                            </div>
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="myEvents">
                            <h3>My events</h3>
                            <div className="events">
                                <EventsList/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    } else {
        return (
            <div className="container">
                <header>
                    <h2>Main Page</h2>
                    <RegistrationBlock/>
                </header>
            </div>
        );
    }
};

export default MainPage;