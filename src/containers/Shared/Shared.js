import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {deleteUser, getSharedUsers, logoutUser, reloadUser} from "../../store/actions";
import {NavLink} from "react-router-dom";

const Shared = (props) => {
    const dispatch = useDispatch();
    const sharedUsers = useSelector(state => state.user.sharedUsers);
    const user = useSelector(state => state.user.user);

    useEffect(() => {
        dispatch(getSharedUsers(props.match.params.id));
        dispatch(reloadUser(user.email));
    }, [dispatch, props.match.params.id, user.email]);

    useEffect(() => {
        const interval = setInterval( () => {
            dispatch(getSharedUsers(props.match.params.id));
            dispatch(reloadUser(user.email));
            }, 2000);
        return () => clearInterval(interval);
    }, [dispatch, props.match.params.id, user.email]);

    const logOut = () => {
        dispatch(logoutUser(user));
    };

    const deleteUserHandler = (event) => {
        dispatch(deleteUser(event.target.value));
    };

    const getUserEvents = event => {
        props.history.push("/userEvents/" + event.target.id);
    };

    const sharedUsersBlock = user.shared.map((shared, ind) => {
        return (
            <div className="userByInfo" key={ind}>
                <div className="userByInfoInfo">
                    <h5>{shared.name}</h5>
                    <div className="userByInfoImg">
                        <img src={shared.image} alt=""/>
                    </div>
                </div>
                <button
                    className="shareBtn" type="button"
                    value={shared.id} onClick={deleteUserHandler}
                >Delete</button>
            </div>
        );
    });

    return (
        <div className="container">
            <header>
                <h2>Shared Users</h2>
                <span className="nav">
                <NavLink
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer',
                        marginRight: 10,
                    }}
                    to={"/mainPage/" + user._id}
                >
                    Main Page
                </NavLink>
                <NavLink
                    onClick={logOut}
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer'
                    }}
                    to="/"
                >{user.displayName}</NavLink>
                <img className="userImage" src={user.imageLink} alt=""/>
            </span>
            </header>
            <div className="content">
                <div className="sharedBlock">
                    <div className="youShared">
                        <h4>You shared with:</h4>
                        <div className="youSharedBlock">
                            <div className="youSharedUsers">
                                {sharedUsersBlock}
                            </div>
                        </div>
                    </div>
                    <div className="sharedWithYou">
                        <h4>Shared with you:</h4>
                        <div className="sharedWithYouBlock">
                            <div className="sharedWithYouUsers">
                                {sharedUsers && sharedUsers.map((shared, ind) => {
                                    return (
                                        <div className="userByInfo" key={ind}>
                                            <div className="userByInfoInfo">
                                                <h5
                                                    id={shared.id} onClick={getUserEvents}
                                                    style={{textDecoration: "underline"}}
                                                >{shared.name}</h5>
                                                <div className="userByInfoImg">
                                                    <img src={shared.image} alt=""/>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Shared;