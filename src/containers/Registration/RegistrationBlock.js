import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {logoutUser} from "../../store/actions";
import {NavLink} from "react-router-dom";

const RegistrationBlock = () => {
    const user = useSelector(state => state.user.user);
    const dispatch = useDispatch();

    const logOut = () => {
        dispatch(logoutUser(user));
    };

    if (user) {
        return (
            <span className="nav">
                <NavLink
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer',
                        marginRight: 10,
                    }}
                    to={"/shared/" + user._id}
                >
                    Shared
                </NavLink>
                <NavLink
                    onClick={logOut}
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer'
                    }}
                    to="/"
                >{user.displayName}</NavLink>
                <img className="userImage" src={user.imageLink} alt=""/>
            </span>
        )
    } else {
        return (
            <div>
                <NavLink
                    onClick={logOut}
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer'
                    }}
                    to="/"
                >Create Account</NavLink>
            </div>
        )
    }
};

export default RegistrationBlock;