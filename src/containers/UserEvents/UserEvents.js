import React from 'react';
import {logoutUser} from "../../store/actions";
import {useDispatch, useSelector} from "react-redux";
import SharedEventList from "../../components/EventsList/SharedEventList";
import {NavLink} from "react-router-dom";

const UserEvents = props => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.user.user);

    const logOut = () => {
        dispatch(logoutUser(user));
    }

    return (
        <div className="container">
            <header>
                <h2>Shared Users</h2>
                <span className="nav">
                 <NavLink
                     style={{
                         color: 'black',
                         textDecoration: 'underline',
                         cursor: 'pointer',
                         marginRight: 10,
                     }}
                     to={"/shared/" + user._id}
                 >
                    Shared
                </NavLink>
                <NavLink
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer',
                        marginRight: 10,
                    }}
                    to={"/mainPage/" + user._id}
                >
                    Main Page
                </NavLink>
                <NavLink
                    onClick={logOut}
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer'
                    }}
                    to="/"
                >{user.displayName}</NavLink>
                <img className="userImage" src={user.imageLink} alt=""/>
            </span>
            </header>
            <SharedEventList id={props.match.params.id}/>
        </div>
    );
};

export default UserEvents;